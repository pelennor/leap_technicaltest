#!/usr/bin/python3

import unittest
import requests
import json
import yaml


f = open ('testdata.yaml')
v = yaml.load(f)['variables']
f.close()

nameError = '{"error":{"text":SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'employee_name\' cannot be null}}'
salaryError = '{"error":{"text":SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'employee_salary\' cannot be null}}'
ageError = '{"error":{"text":SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'employee_age\' cannot be null}}'

testCreateData = { 'empty' : ({} , nameError),
             'nameOnly' : ({'name' : 'Name Only'}, salaryError),
             'salaryOnly': ({'salary' : '99000'}, nameError),
             'ageOnly': ({'age' : '99'}, nameError),
             'salaryAndAge': ({'salary': '34567', 'age': '23'}, nameError),
             'nameAndSalary': ({'name' : 'Name And Salary', 'salary': '64234'}, ageError),
             'nameAndAge': ({'name' : 'Name And Age', 'age': '42'}, salaryError)
           }
testCreateThenDelete = {'name' : 'Test createThenDelete', 'salary' : '26432', 'age' : '22'}

largeIDNeverAllocated = '9999999999'

class testNonJSONResponses(unittest.TestCase):

    # Helper function for testing against a record that has been deleted
    def createThenDelete(self):
        response = requests.post(v['url_create'], json=testCreateThenDelete)
        rvars = json.loads(response.content)
        returnedID = rvars['id']
        response2 = requests.delete(v['url_delete']+'/'+returnedID)
        return returnedID

    
    def checkNotFoundResponse(self, response):
        if response.text == 'false' and response.status_code == 200 :
            return True
        else :
            return False


    # Binary search of the result set to check for a given ID
    def searchGetResultByIDBusted(self, getResult, inID):
        IDWanted = int(inID)
        topIdx = 0
        botIdx = len(getResult)
        while botIdx <= topIdx:
            testIdx = (topIdx + botIdx) / 2
            testID = int(getResult[testIdx]['id'])
            if testID == IDWanted :
                return getResult[testIdx]
            elif testID > IDWanted :
                topIdx = testIdx - 1
            else :
                botIdx = testIdx + 1

    def searchGetResultByID(self, getResult, inID):
        searchIdx = -1
        endIdx = -len(getResult)

        while searchIdx > endIdx :
            if getResult[searchIdx]['id'] == inID:
                return getResult[searchIdx]
            searchIdx -= 1
        return None


    def testCreateWithNonJSONResponses(self):
        for testScenario in testCreateData.keys() :
            inData, expectedError = testCreateData[testScenario]
            print('Testing create scenario %s' % (testScenario))
            response = requests.post(v['url_create'], json=inData)
            self.assertEqual(response.text, expectedError)

    def testGet1AfterDelete(self):
        myID=self.createThenDelete()
        response = requests.get(v['url_get1']+'/'+myID)
        self.assertTrue(self.checkNotFoundResponse(response))

    def testDeleteAfterDelete(self):
        myID=self.createThenDelete()
        response = requests.delete(v['url_delete']+'/'+myID)
        self.assertTrue(self.checkNotFoundResponse(response))

    def testUpdateAfterDelete(self):
        myID=self.createThenDelete()
        dictForUpdate = testCreateData['salaryAndAge'][0]
        response = requests.put(v['url_update']+'/'+myID, json=dictForUpdate)
        self.assertTrue(self.checkNotFoundResponse(response))

    def testGet1NeverExisted(self):
        response = requests.get(v['url_get1']+'/'+largeIDNeverAllocated)
        self.assertTrue(self.checkNotFoundResponse(response))

    def testDeleteNeverExisted(self):
        response = requests.delete(v['url_delete']+'/'+largeIDNeverAllocated)
        self.assertTrue(self.checkNotFoundResponse(response))

    def testUpdateNeverExisted(self):
        dictForUpdate = testCreateData['salaryAndAge'][0]
        response = requests.put(v['url_update']+'/'+largeIDNeverAllocated,json=dictForUpdate)
        self.assertTrue(self.checkNotFoundResponse(response))


    def testGetAfterCreate(self):
        response = requests.post(v['url_create'], json=testCreateThenDelete)
        rvars1 = json.loads(response.content)
        returnedID = rvars1['id']
        response2 = requests.get(v['url_get'])
        rvars2=json.loads(response2.content)
        dataFromGet = self.searchGetResultByID(rvars2, returnedID)

        # Now we do a get1 to compare since the variable names are different on the gets compared to the creates
        response3=requests.get(v['url_get1']+'/'+returnedID)
        dataToCompare = json.loads(response3.content)
        self.assertEqual(dataFromGet, dataToCompare)

    def testGetAfterUpdate(self):
        response = requests.post(v['url_create'], json=testCreateThenDelete)
        rvars1 = json.loads(response.content)
        returnedID = rvars1['id']
        dictForUpdate = testCreateData['salaryAndAge'][0]
        response2 = requests.put(v['url_update']+'/'+returnedID, json=dictForUpdate)
   
        response3 = requests.get(v['url_get'])
        rvars3=json.loads(response3.content)
        dataFromGet = self.searchGetResultByID(rvars3, returnedID)

        # Now we do a get1 to compare since the variable names are different on the gets compared to the creates
        response4=requests.get(v['url_get1']+'/'+returnedID)
        dataToCompare = json.loads(response4.content)
        self.assertEqual(dataFromGet, dataToCompare)
        
    def testGetAfterDelete(self):
        myId=self.createThenDelete()
        response=requests.get(v['url_get'])
        rvars=json.loads(response.content)
        self.assertEqual(self.searchGetResultByID(rvars, myId), None)

if __name__ == '__main__' :
    unittest.main()
