Prerequisites for running tests:
================================

+ Python 3
+ Additional Python modules:
  * pytest
  * tavern
  * pyyaml

Has been tested running on Linux( Ubuntu 18.04 LTS), should also work on Windows in theory (will probably need Cygwin with all the above set up under it to be able to use the runtests and test_nonJson.py executables unchanged)

I've also got it all set up and ready to go under a Virtualbox VM, so if it's easier to just compress and upload a copy of that, let me know and I'll set it up.

How to install the above
========================
(on Ubuntu, should be similar for other distributions)

sudo apt-get install python3 python3-setuptools
sudo pip install -U pytest
sudo pip install tavern
sudo pip install pyyaml

Running the tests
=================

./runtests

If there are any issues, try the following command to ensure two of the modules are executuable prior to rerunning runtests :

chmod +x runtests test_nonJson.py


Note there should be 4 failed test cases in the output - this is a result of the update and delete functions not respecting the same conventions as the get one does for deleted employees.  To be specific, the get one returns a 200 error and 'false' in the body when called with a non existent or deleted ID, whereas update and delete have the same behaviour across deleted and non deleted employees.  In my opinion of course, the functions should be consistent with each other in the way they treat deleted employee records, and this is not currently the case.

